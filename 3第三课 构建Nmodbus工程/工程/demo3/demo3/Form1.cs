﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Modbus.IO;
using Modbus.Device;
using System.IO.Ports;

namespace demo3
{
    public partial class Form1 : Form
    {
        private SerialPort sp = null;
        private ModbusSerialMaster msm;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            sp = new SerialPort("COM1", 9600);
            sp.Open();
            msm = ModbusSerialMaster.CreateRtu(sp);

            ///
            //使用msm去读写modbus设备
            // ...
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (sp.IsOpen)
            {
                sp.Close();
            }
        }
    }
}
