﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        

        private static void ProcessDnsInformation(IAsyncResult ar)
        {
            Console.WriteLine((string)ar.AsyncState);
        }

        string host;
        static void Main(string[] args)
        {
            AsyncCallback callBack = new AsyncCallback(ProcessDnsInformation);
            Dns S = new Dns();
            
            S.BeginGetHostEntry("123", callBack, "456");

            //Action fn = Run;
            //fn.BeginInvoke(callBack, null);
        }

        private static void Run()
        {
            Console.WriteLine("Run");
        }
    }
    public  class Dns:IAsyncResult
    {
        private static AsyncCallback m_AsyncCallback;

        protected AsyncCallback AsyncCallback
        {
            get
            {
                return m_AsyncCallback;
            }

            set
            {
                m_AsyncCallback = value;
            }
        }

        private bool m_IsCompleted;

        public bool IsCompleted
        {
            get
            {
                return m_IsCompleted;
            }
        }

        public WaitHandle m_AsyncWaitHandle;

        public WaitHandle AsyncWaitHandle
        {
            get
            {
                return m_AsyncWaitHandle;
            }
        }

        public object m_AsyncState ;
 
        public object AsyncState
        {
            get
            {
                return m_AsyncState;
            }
        }


        private bool m_CompletedSynchronously ;

        public bool CompletedSynchronously
        {
            get
            {
                return m_CompletedSynchronously;
            }
        }

        public  IAsyncResult BeginGetHostEntry(string a, AsyncCallback b, object c)
        {
            m_AsyncState = c;

            m_AsyncCallback = b;
                b(this);
           // m_AsyncCallback(this);
            m_IsCompleted = true;


            return this;
        }


    }
}
